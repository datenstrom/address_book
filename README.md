# Address Book

The [go protocol buffers tutorial](https://developers.google.com/protocol-buffers/docs/gotutorial).

## Resources

- [godoc](https://blog.golang.org/godoc-documenting-go-code)
- [gitlab ci](https://about.gitlab.com/2017/11/27/go-tools-and-gitlab-how-to-do-continuous-integration-like-a-boss/)
